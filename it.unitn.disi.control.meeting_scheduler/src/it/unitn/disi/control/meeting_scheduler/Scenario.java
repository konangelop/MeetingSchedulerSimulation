package it.unitn.disi.control.meeting_scheduler;

public class Scenario {

	public void increaseMaxParticipants(int n){
		Simulation.maxNoP=Simulation.maxNoP+n;
	}
	
	public void decreaseMaxParticipants(int n){
		Simulation.maxNoP=Simulation.maxNoP-n;
	}
	
	public void increaseMinParticipants(int n){
		Simulation.minNoP=Simulation.minNoP+n;
	}
	
	public void decreaseMinParticipants(int n){
		Simulation.minNoP=Simulation.minNoP+n;
	}
	
	public void increaseHotelPrice(int c){
		Simulation.hotelPrice=Simulation.hotelPrice+c;
	}
	
	public void decreaseHotelPrice(int c){
		Simulation.hotelPrice=Simulation.hotelPrice-c;
	}
	
	public void increaseLocalRooms(int x){
		Simulation.NoLR=Simulation.NoLR+x;
	}
	
	public void increaseHotelRooms(int x){
		Simulation.NoHR=Simulation.NoHR+x;
	}
	
	public void decreaseLocalRooms(int x){
		Simulation.NoLR=Simulation.NoLR-x;
	}
	
	public void decreaseHotelRooms(int x){
		Simulation.NoHR=Simulation.NoHR-x;
	}
	
	public void decreaseAvailability(int x){
		Simulation.availableProbability=Simulation.availableProbability-x;
		Simulation.availableProbabilityIgnored=Simulation.availableProbabilityIgnored-x;
	}
	
	public void increaseAvailability(int x){
		Simulation.availableProbability=Simulation.availableProbability+x;
		Simulation.availableProbabilityIgnored=Simulation.availableProbabilityIgnored+x;
	}
	
	public void increaseMCA(int x){
		Simulation.MCA=Simulation.MCA+x;
	}
	
	public void decreaseMCA(int x){
		Simulation.MCA=Simulation.MCA-x;
	}
	
	public void increaseNoM(int n){
		Simulation.NoM +=n;
	}
	
	public void decreaseNoM(int n){
		Simulation.NoM -=n;
	}
	
	public void scheduleAutomatically(){
		Simulation.dateSelectionMode=1;
	}	
	
	public void scheduleManually(){
		Simulation.dateSelectionMode=0;
	}
		
	public void increaseFhM(int n){
		Simulation.FhM+=n;
	}
	
	public void decreaseFhM(int n){
		Simulation.FhM-=n;
	}
	
 	public void setupSimulation1(int day){
		switch(day){
		
		case 30: 	Simulation.NoM=Simulation.NoM+10;
					break;
		case 35: 	Simulation.NoM=Simulation.NoM+10;
					break;
		case 40: 	Simulation.NoM=Simulation.NoM+10;
					break;
		case 45: 	Simulation.NoM=Simulation.NoM+15;
					break;
		case 50: 	Simulation.NoM=Simulation.NoM+15;
					break;
		case 55: 	Simulation.NoM=Simulation.NoM+25;
					break;
		case 60: 	Simulation.NoM=Simulation.NoM+25;
					break;
		case 65: 	Simulation.NoM=Simulation.NoM+25;
					break;
		case 70: 	Simulation.NoLR=Simulation.NoLR+1;
					break;
		case 75: 	Simulation.NoLR=Simulation.NoLR+2;
					break;
		case 80: 	Simulation.NoLR=Simulation.NoLR+3;
					break;
		case 85: 	Simulation.NoLR=Simulation.NoLR+4;
					break;
		case 90: 		Simulation.NoLR=Simulation.NoLR+5;
					break;
		case 95: 	Simulation.NoM=Simulation.NoM-25;
					break;
		case 100:	Simulation.NoM=Simulation.NoM-25;
					break;
		case 105: 	Simulation.NoM=Simulation.NoM-25;
					break;
		case 110:	Simulation.NoM=Simulation.NoM-25;
					increaseHotelPrice(10);
					break;
		case 115: 	increaseHotelPrice(5);
					decreaseAvailability(5);
					break;
		case 120:	increaseHotelPrice(10);
					decreaseAvailability(10);
					break;
		case 125: 	increaseHotelPrice(10);
					decreaseAvailability(5);
					break;
		case 160:	decreaseHotelRooms(10);
					//increaseMCA(2);
					break;
		case 170: 	//increaseMCA(5);
					break;
		case 190:	//decreaseMCA(7);
					break;
		
		}

	}
	
	public void setupSimulation2(int day){
		switch(day){
		
		case 30: 	Simulation.NoM=Simulation.NoM+20;
					break;
		case 35: 	Simulation.NoM=Simulation.NoM+20;
					break;
		case 40: 	Simulation.NoM=Simulation.NoM+20;
					break;
		case 45: 	Simulation.NoM=Simulation.NoM+25;
					break;
		case 50: 	Simulation.NoM=Simulation.NoM+25;
					break;
		case 55: 	Simulation.NoM=Simulation.NoM+25;
					break;
		case 60: 	Simulation.NoM=Simulation.NoM+25;
					break;
		case 65: 	Simulation.NoM=Simulation.NoM+25;
					break;
		case 70: 	//Simulation.NoLR=Simulation.NoLR+1;
					break;
		case 75: 	//Simulation.NoLR=Simulation.NoLR+2;
					break;
		case 80: 	//Simulation.NoLR=Simulation.NoLR+3;
					break;
		case 85: 	//Simulation.NoLR=Simulation.NoLR+4;
					break;
		case 90: 	//	Simulation.NoLR=Simulation.NoLR+5;
					break;
		case 95: 	//Simulation.NoM=Simulation.NoM-25;
					break;
		case 100:	//Simulation.NoM=Simulation.NoM-25;
					break;
		case 105: 	//Simulation.NoM=Simulation.NoM-25;
					break;
		case 110:	//Simulation.NoM=Simulation.NoM-25;
					increaseHotelPrice(10);
					break;
		case 115: 	increaseHotelPrice(5);
					decreaseAvailability(5);
					break;
		case 120:	increaseHotelPrice(10);
					decreaseAvailability(10);
					break;
		case 125: 	increaseHotelPrice(10);
					decreaseAvailability(5);
					break;
		case 160:	//decreaseHotelRooms(10);
					//increaseMCA(2);
					break;
		case 170: 	//increaseMCA(5);
					break;
		case 190:	//decreaseMCA(7);
					break;
		
		}

	}
	
	public void setupSimulation3(int day){
		switch(day){
		case 5: 	increaseNoM(15);
					break;
		case 8: 	increaseNoM(15);
					break;
		case 14: 	increaseNoM(25);
					break;		
		case 20:	increaseNoM(25);
		
		case 75: 	increaseNoM(30);
					increaseLocalRooms(2);
					increaseHotelRooms(2);
					break;
		case 80: 	increaseNoM(15);
					break;
		case 82:	increaseLocalRooms(10);
					break;
		case 90:	increaseHotelRooms(10);			
					break;
		case 100: 	increaseLocalRooms(20);
					break;
		case 105: 	decreaseNoM(80);
					break;
		case 110: 	decreaseAvailability(20);
					break;
		case 250: 	scheduleAutomatically();
					break;
		case 350: 	scheduleManually();
					break;
		case 450: 	scheduleAutomatically();
					break;
		case 500:	increaseMCA(2);
					break;
		case 550: 	increaseMCA(2);
					break;
		case 600:	increaseHotelPrice(17);
					break;
		case 750: 	decreaseHotelRooms(10);
					break;
		case 800: 	decreaseFhM(40);
					break;
		case 850: 	decreaseAvailability(20);
					break;
		case 950: 	increaseFhM(40);
					break;
		case 1000: 	decreaseMCA(3);
					break;
		case 1050:	scheduleManually();
					break;
		
					
		}
	}
	public void setupSimulation4(int day){
		switch(day){
		case 5: 	increaseNoM(15);
					break;
		case 8: 	increaseNoM(15);
					break;
		case 14: 	increaseNoM(25);
					break;		
		case 20:	increaseNoM(25);
		
		case 75: 	increaseNoM(30);
					//increaseLocalRooms(2);
					//increaseHotelRooms(2);
					break;
		case 80: 	increaseNoM(15);
					break;
		case 82:	//increaseLocalRooms(10);
					break;
		case 90:	//increaseHotelRooms(10);			
					break;
		case 100: 	//increaseLocalRooms(20);
					break;
		case 105: 	decreaseNoM(80);
					break;
		case 110: 	decreaseAvailability(20);
					break;
		case 250: 	//scheduleAutomatically();
					break;
		case 350: 	scheduleManually();
					break;
		case 450: 	//scheduleAutomatically();
					break;
		case 500:	//increaseMCA(2);
					break;
		case 550: 	//increaseMCA(2);
					break;
		case 600:	increaseHotelPrice(17);
					break;
		case 750: 	//decreaseHotelRooms(10);
					break;
		case 800: 	//decreaseFhM(40);
					break;
		case 850: 	decreaseAvailability(20);
					break;
		case 950: 	//increaseFhM(40);
					break;
		case 1000: 	//decreaseMCA(3);
					break;
		case 1050:	//scheduleManually();
					break;
		
		}
	}	
}
