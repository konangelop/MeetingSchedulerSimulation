package it.unitn.disi.control.meeting_scheduler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import org.jfree.ui.ApplicationFrame;

public class Simulation extends ApplicationFrame{
	
	public Simulation(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	//Controller switch
	static boolean controllerOn=true;
	//Control Input
	static int NoHR=10; 			//The number of hotel reservations allowed
	static int NoLR=25;				//The number of local rooms available for meetings
	static int FhM = 80;			//From how many participants (percentage %) the timetables must be collected
	static int MCA=0;				//maximum conflicts allowed
	static int dateSelectionMode=0;	//0 when date is selected manually ||  1 when date is selected by the system 
	
	//Environment variables
	static int NoM=190; 			//The number of meetings in a day
	static int hotelPrice=30;		//The cost of a hotel reservation
	static int localRoomCost=10;  	//The cost of a local room per day
	static int availableProbability=99; // The probability (0-100%) of a participant to be available for a meeting 
	static int availableProbabilityIgnored =availableProbability-10; //The probability of a participant who 
	static int minNoP=7;			//minimum number of participants
	static int maxNoP=19;			//maximum number of participants
									//timetable was not collected to show up

	//Simulation variables
	static int simDuration=1200;	//The number of days the simulation must last		
	static int NoHR_done=0; 		//The number of hotel reservations done
	static int[] day = new int[9]; 	//A day has 8 meeting slots of 1 hour each
	static int availableP=0;		//The available participants number in a meeting 
	
	//Indicators - Measured Output
	static double I1=0; 			//success rate of low cost
	static double I2=0;       		//success rate of find rooms
	static double I3=0;				//success rate of find date
	static double I4=0;				//success rate of high participation
	static double I5=0;  			//success rate of fast scheduling
	
	//Indicators - reference inputs
	static double Iref1=75.0;
	static double Iref2=90.0;
	static double Iref3=90.0;
	static double Iref4=75.0;
	static double Iref5=75.0;
	
	//variables for measuring the indicators
	static int totalCost=0;		
	static int totalMeetings=0;
	static int totalMeetingsDateFound=0;
	static int totalMeetingsRoomsFound=0;
	static int totalMeetingsHighParticipationAchieved=0;
	static int totalDaysWithinBudget=0;	
	static int Budget=1400;									// The daily budged for meetings
	static int highParticipationThresshold=75;

	static boolean dateFound(int nop){
		
	    Random randomGenerator = new Random();
	    availableP=0;
		for(int i=1;i<=nop;i++){
		      int randomInt = randomGenerator.nextInt(101);
		      if(randomInt<=availableProbability) availableP++;
		}
		
		if(dateSelectionMode==0) return true;

//		System.out.println("For this meeting "+availableP+" out of "+nop+" can participate.");
		if((nop-availableP)<=MCA){
			return true;
		}
		else return false;
	}
	
	boolean roomFound(int p){
		if(NoHR+NoLR>=p) return true;
		else {
			//System.out.println("For this slot "+p-(NoHR+NoLR)+" meetings couldn't be scheduled");
			return false;
		}
				
	}
		
	static void assignMeetings2Slots(int mtgs){
	    
		for(int i=1; i<=7; i++){
		    int randomNum = (int)(Math.random() * ((mtgs/2) + 1));
		    day[i]=randomNum;
		    System.out.println("For slot "+i+" we received "+randomNum+" requests.");
		    mtgs=mtgs-randomNum;
		    if(mtgs<0) mtgs=0;		    
		}
		day[8]=mtgs;
	    System.out.println("For slot 8 we received "+mtgs+" requests.");

	}
	
	static	void bookMeetings(){
		totalCost=0;
		NoHR_done=0;
		for(int i=1;i<=8;i++){
			int mtgs = day[i];
			System.out.print("Out of "+mtgs+" meetings ");
			if(mtgs==0) {
				System.out.println("No meetings for this slot.");
				continue;
			}
			int count=0;	//counts for every slot for how many meetings we found a date
			int countRoomsNeeded=0;
			for(int j=1;j<=mtgs;j++){
				availableP=0;
				int participants = minNoP + (int)(Math.random() * ((maxNoP - minNoP) + 1));
//				System.out.println("For this meeting "+participants+" have been invited.");
				int realCollected = (int) Math.ceil(FhM*participants/100.0);
//				System.out.println("We collected timetable for "+realCollected+" of them.");
				boolean found=false;
				if(dateFound(realCollected)){
					count++;
					totalMeetingsDateFound++;
					found=true;
				}
				else day[i]--;
				if((NoHR+NoLR)<j) continue;
				else if(found){					
					totalMeetingsRoomsFound++;
					countRoomsNeeded++;					
					
					int restOfParticipants=participants-realCollected;
				    Random randomGenerator = new Random();
					if(restOfParticipants>=1){
						for(int k=1;k<=restOfParticipants;k++){
							int randomInt = randomGenerator.nextInt(101);
						      if(randomInt<=availableProbabilityIgnored) availableP++;
						}
					}
					double participationPercentage = ((double)availableP/(double)participants)*100;
					if(participationPercentage>=highParticipationThresshold){
						totalMeetingsHighParticipationAchieved++;
					}
				}
			}
			System.out.println("we found dates for "+count+" of them.");
			if(countRoomsNeeded>NoLR) NoHR_done=NoHR_done+(countRoomsNeeded-NoLR);
		}
		totalCost=totalCost+NoLR*localRoomCost+NoHR_done*hotelPrice;
		System.out.println("The total cost for today's meetings is "+totalCost);
		if(totalCost<=Budget) totalDaysWithinBudget++;
		
	}
	
	private static final long serialVersionUID = 1L;

	
	public static void runApp() throws IOException{
		try {
			FileWriter writer = new FileWriter("/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/record.csv");
		
			writer.append("MCA");
		    writer.append(',');
		    writer.append("NoLR");
		    writer.append(',');
		    writer.append("NoHR");
		    writer.append(',');
		    writer.append("FhM");
		    writer.append(',');
		    writer.append("dateSelectionMode");
		    writer.append(',');
		    writer.append("I1");
		    writer.append(',');
		    writer.append("I2");
		    writer.append(',');
		    writer.append("I3");
		    writer.append(',');
		    writer.append("I4");
		    writer.append(',');
		    writer.append("Iref1");
		    writer.append(',');
		    writer.append("Iref2");
		    writer.append(',');
		    writer.append("Iref3");
		    writer.append(',');
		    writer.append("Iref4");
		    writer.append('\n');
		
		    Scenario sc1 = new Scenario();
		    System.out.println("Simulation Begins");
		    
		for(int i=1;i<=simDuration;i++){					
			
			sc1.setupSimulation4(i);
			//getSignal();
			
			totalMeetings=totalMeetings+NoM;
			assignMeetings2Slots(NoM);
			bookMeetings();
			I1 = (int) Math.ceil( ((double)totalDaysWithinBudget/(double)i)*100);
			I2 = (int) Math.ceil( ((double)totalMeetingsRoomsFound/(double)totalMeetingsDateFound)*100);
			I3 = (int) Math.ceil( ((double)totalMeetingsDateFound/(double)totalMeetings)*100);
			I4 = (int) Math.ceil( ((double)totalMeetingsHighParticipationAchieved/(double)totalMeetingsRoomsFound)*100);

			System.out.println("We received "+totalMeetings+" meeting requests in total up to now.");
			System.out.println("We found a date successfully for "+totalMeetingsDateFound+" of them.");
			System.out.println("The days within the allowed budged were "+totalDaysWithinBudget );

			System.out.println("I1 = "+I1+"%" );
			System.out.println("I2 = "+I2+"%" );
			System.out.println("I3 = "+I3+"%" );
			System.out.println("I4 = "+I4+"%" );

	
			
			writer.append(Integer.toString(MCA));
		    writer.append(',');
		    writer.append(Integer.toString(NoLR));
		    writer.append(',');
		    writer.append(Integer.toString(NoHR));
		    writer.append(',');
		    writer.append(Integer.toString(FhM));
		    writer.append(',');
		    writer.append(Integer.toString(dateSelectionMode));
		    writer.append(',');
		    writer.append(Double.toString(I1));
		    writer.append(',');
		    writer.append(Double.toString(I2));
		    writer.append(',');
		    writer.append(Double.toString(I3));
		    writer.append(',');
		    writer.append(Double.toString(I4));
		    writer.append(',');
		    writer.append(Double.toString(Iref1));
		    writer.append(',');
		    writer.append(Double.toString(Iref2));
		    writer.append(',');
		    writer.append(Double.toString(Iref3));
		    writer.append(',');
		    writer.append(Double.toString(Iref4));
		    writer.append('\n');
			
		    //createOutput(I1,I2,I3,I4);
		    //getInput();
		}
		
		writer.flush();
	    writer.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ProcessBuilder builder = new ProcessBuilder("python", "/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/print.py");
        builder.inheritIO(); 
        final Process process = builder.start();
	}
	
	
	public static void createOutput(double out1, double out2, double out3, double out4) throws IOException{
		try{
		  System.out.println("Creating new output");
		  String outputPath = "/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/output.csv";
  		  FileWriter writer = new FileWriter(outputPath,false);
  		 
  		  writer.append(Double.toString(out1));
  		  writer.append(',');
  		  writer.append(Double.toString(out2));
  		  writer.append(',');
  		  writer.append(Double.toString(out3));
  		  writer.append(',');
  		  writer.append(Double.toString(out4));
  		  writer.append('\n');
  		  writer.flush();
  		  writer.close();
  		  System.out.println("New output created");
  	  }catch (IOException e) {
  		  System.out.println("File not found or couldn't be created");
  		  e.printStackTrace();
  	  }
		FileWriter output = new FileWriter("/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/signal.txt", false);
        String semaphore = "1";
        output.write(semaphore);
        output.flush();
        output.close();
		
	}
	
	
	public static void getSignal() throws IOException{
		System.out.println("Application is reading the sempaphore");	
	      File signal = new File("/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/signal.txt");	      
	      int sv=0;
	      
	      while(true){
	    	  FileReader fr = new FileReader(signal); 
	          char [] a = new char[50];
	          fr.read(a);
	          sv = Character.getNumericValue(a[0]);
	          fr.close();
	          
	          if(sv==0){
	        	  System.out.println("Application is running");
	        	  //write output file        
	        	  System.out.println("Producing new output");
	        	  	        	  
	        	  break;
	          }
	      }
		
	}
	
	public static void getInput() throws IOException{
		
	      File signal = new File("/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/signal.txt");	      
	      int sv=0;
	      //Random randomGenerator = new Random();
	      System.out.println("Waiting for new control signal...");
	      while(true){
	    	  FileReader fr = new FileReader(signal); 
	          char [] a = new char[50];
	          fr.read(a);
	          sv = Character.getNumericValue(a[0]);
	          fr.close();
	          
	          if(sv==2){
	        	  System.out.println("New control signal available!");
	        	  
	        	  String csvFile = "/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/input.csv";
	        	  BufferedReader br = null;
	        	  String line = "";
	        	  String cvsSplitBy = ",";
	        	  try {

	        			br = new BufferedReader(new FileReader(csvFile));
	        			while ((line = br.readLine()) != null) {

	        				String[] controlInput = line.split(cvsSplitBy);

	        				System.out.println("input1= " + controlInput[0] 
	        	                                 + " , input2= " + controlInput[1] 
	        	                                 + " , input3= " + controlInput[2]
	        	                                 + " , input4= " + controlInput[3]
	        	                                 + " , input5= " + controlInput[4]);
	        				float cv1 = Float.parseFloat(controlInput[0]);
	        				float cv2 = Float.parseFloat(controlInput[1]);
	        				float cv3 = Float.parseFloat(controlInput[2]);
	        				float cv4 = Float.parseFloat(controlInput[3]);
	        				float cv5 = Float.parseFloat(controlInput[4]);
	        				
	        				if(controllerOn){
	        					MCA = (int) Math.ceil(cv1);
	        					System.out.println("New MCA = "+MCA);
	        					NoLR =(int) Math.ceil(cv2);
	        					System.out.println("New NoLR = "+NoLR);
	        					NoHR = (int) Math.ceil(cv3);
	        					System.out.println("New NoHR = "+NoHR);
	        					FhM = 100;//(int) Math.ceil(cv4 );
	        					System.out.println("New FhM = "+FhM);
	        					dateSelectionMode = (int) Math.ceil(cv5);
	        					System.out.println("New dateSelectionMode = "+dateSelectionMode);
	        				}
	        			}
	        			FileWriter output = new FileWriter("/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/signal.txt", false);
	        	          String semaphore = "0";
	        	          output.write(semaphore);
	        	          output.flush();
	        	          output.close();

	        		} catch (FileNotFoundException e) {
	        			e.printStackTrace();
	        		} catch (IOException e) {
	        			e.printStackTrace();
	        		} finally {
	        			if (br != null) {
	        				try {
	        					br.close();
	        				} catch (IOException e) {
	        					e.printStackTrace();
	        				}
	        			}
	        		}

	        	  	        	  
	        	  break;
	          }
	      }
		
	}
	

	
	public static void main(String[] args) throws IOException {
		String path1="/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/record.csv";
		String path2="/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/output.csv";
		String path3="/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/input.csv";
		File f1 = new File(path1);
		File f2 = new File(path2);
		File f3 = new File(path3);
		f1.delete();
		f2.delete();
		f3.delete();
		
		ProcessBuilder builder = new ProcessBuilder("python", "/Users/konangelop/Dropbox/2016SEAMS-mpc/python/mpc_meeting_scheduler_integrated.py");
        builder.inheritIO(); 
        final Process process = builder.start();
		System.out.println("The controller is deployed");
		runApp();
		process.destroy();
	}

}
