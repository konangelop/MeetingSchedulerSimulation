import numpy as np
import matplotlib.pyplot as plt

data=np.genfromtxt('/Users/konangelop/git/MeetingSchedulerSimulation/it.unitn.disi.control.meeting_scheduler/logs/record.csv',skiprows=1,delimiter=',')
mca=data[:,0]
nolr=data[:,1]
nohr=data[:,2]
fhm=data[:,3]
mode=data[:,4]
i1=data[:,5]
i2=data[:,6]
i3=data[:,7]
i4=data[:,8]
i1r=data[:,9]
i2r=data[:,10]
i3r=data[:,11]
i4r=data[:,12]

#print data

time = range(0,1200)
print mca
print nolr
print len(i1)
print len(time)
print len(mca)
print len(nolr)
plt.subplot(2,1,1)
plt.plot(time, i1, color='r', label='I1')
plt.plot(time, i2, color='b', label='I2')
plt.plot(time, i3, color='g', label='I3')
plt.plot(time, i4, color='m', label='I4')
plt.plot(time, i1r, color='r', linestyle='--',  label='I1ref')
plt.plot(time, i2r, color='b',  linestyle='--',  label='I2ref')
plt.plot(time, i3r, color='g', linestyle='--',  label='I3ref')
plt.plot(time, i4r, color='m', linestyle='--',  label='I4ref')
plt.xlabel('time')
plt.xticks(np.arange(min(time), max(time)+1, 50.0))
plt.ylabel('Indicators')
plt.ylim([0,110])
plt.title('Outputs')
plt.legend()
#plt.show()

plt.subplot(2,1,2)
plt.plot(time, mca, label='MCA')
plt.plot(time, nolr, label='NoLR')
plt.plot(time, nohr, label='NoHR')
plt.plot(time, fhm, label='FhM')
plt.plot(time, mode, label='dateSelectionMode')
#plt.plot(time, i2, marker='o', linestyle='--', color='r', label='I2')
plt.xlabel('time')
plt.ylabel('Knobs')
plt.title('Inputs')
plt.legend()
plt.show()
